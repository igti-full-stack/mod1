let tabCountries = null
let tabFavorites = null
let allCountries = []
let favoriteCountries = []
let countCountries = 0
let countFavorites = 0
let totalPopulationList = 0
let totalPopulationFavorites = 0

let numberFormat = null

window.addEventListener('load', () => {  
  tabCountries = document.querySelector('#tab-countries')
  tabFavorites = document.querySelector('#tab-favorites')
  countCountries = document.querySelector('#count-countries')
  countFavorites = document.querySelector('#count-favorites')
  totalPopulationList = document.querySelector('#total-population-list')
  totalPopulationFavorites = document.querySelector('#total-population-favorites')

  numberFormat = Intl.NumberFormat('pt-BR')
  
  fetchCountries()
})

async function fetchCountries() {
  const url = `http://172.20.0.2:3001/countries`
  
  const res = await fetch(url)
  const json = await res.json()
  
  allCountries = json.map(country => {
    const { numericCode, translations, population, flag } = country
    return {
      id: numericCode,
      name: translations.pt,
      population,
      formattedPopulation: formatNumber(population),
      flag,
    }
  })
  
  render()
}

function render() {
  renderCountryList()
  renderFavorites()
  renderSummary()

  handleCountryButtons()
}

function renderCountryList() {
  renderList(allCountries, tabCountries)
}

function renderFavorites() {
  renderList(favoriteCountries, tabFavorites)
}

function renderList(fromList, tabList) {
  let renderListHTML = '<div>'

  fromList.forEach(country => {
    const { id, name, formattedPopulation, flag } = country
    const favoriteHTML = `
      <div class="country">
        <div>
          <a id="${id}" class="waves-effect waves-light btn">+</a>
        </div>
        <div>
          <img src="${flag}" alt="${name}" />
        </div>
        <div>
          <ul>
            <li>${name}</li>
            <li>${formattedPopulation}</li>
          </ul>
        </div>        
      </div>
    `

    renderListHTML += favoriteHTML
  })

  renderListHTML += '</div>'

  tabList.innerHTML = renderListHTML
}

function renderSummary() {
  const totalPopulation = getTotalPopulation(allCountries)
  const totalFavorites = getTotalPopulation(favoriteCountries)
  
  countCountries.textContent = lengthCountries(allCountries)
  countFavorites.textContent = lengthCountries(favoriteCountries)
  totalPopulationList.textContent = formatNumber(totalPopulation)
  totalPopulationFavorites.textContent = formatNumber(totalFavorites)
} 

function lengthCountries(countries) {
  return countries.length
}

function getTotalPopulation(countries) {
  return countries.reduce((accumulator, current) => {
    return accumulator + current.population
  }, 0)
}

function handleCountryButtons() {
  const countryButtons = Array.from(tabCountries.querySelectorAll('.btn'))
  const favoriteButtons = Array.from(tabFavorites.querySelectorAll('.btn'))

  isFavorite(countryButtons, false)
  isFavorite(favoriteButtons, true)
}

function isFavorite(countries, favorite) {
  countries.forEach(button => {
    button.addEventListener('click', () => {
      if(favorite) {
        removeFromFavorites(button.id)
      } else {
        addToFavorites(button.id)
      }
    })
  })
}

function addToFavorites(id) {
  handleFavorites(id, allCountries, favoriteCountries)
}

function removeFromFavorites(id) {
  handleFavorites(id, favoriteCountries, allCountries)
}

function handleFavorites(id, countriesList, otherList) {
  const countryToMove = countriesList.find(button => button.id === id)
  
  otherList.push(countryToMove)
  otherList.sort((a,b) => a.name.localeCompare(b.name))
  
  countriesList.filter((country, index) => {
    if(country.id !== id) {
      return true
    }
    countriesList.splice(index, 1)    
  })

  render()
}

function formatNumber(number) {
  return numberFormat.format(number)
}
